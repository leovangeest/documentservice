# documentservice

This service provides access to documents.
It has entrypoints to store and retrieve documents.

The API of this service is defined in an openapi 3.0.0 yaml format.

[This specification is here](src/main/resources/openapi.yaml)
## resources

[Smartbear openapi 3.0 video](https://www.youtube.com/watch?v=6kwmW_p_Tig)

A very good video, introducing openapi 3

[Swagger documentation](https://swagger.io/docs/)

All documentation about openapi 3

[Developing RESTful APIs with JAX-RS](https://www.youtube.com/playlist?list=PLqq-6Pq4lTTZh5U8RbdXq0WaYvZBz2rbn)

A course with a good introduction to what rest is.

[Advanced JAX-RS](https://www.youtube.com/watch?v=aHGGMX_Zq1w&list=PLqq-6Pq4lTTY40IcG584ynNqibMc1heIa)

The second part of this course.

## Maven plugin
This project uses the openapi code generator maven plugin.

Documentation for this plugin can be found [on this github page](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-maven-plugin)
