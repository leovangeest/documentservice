package nl.marlevous.documentservice.model;

public class DocumentCount {
  private Long count;

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }
}
