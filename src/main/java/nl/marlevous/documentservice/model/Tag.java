package nl.marlevous.documentservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity

@XmlRootElement
public class Tag {

  @Id
  private String name;
  @Transient
  private String documentsUrl;
  @OneToMany
  private List<Document> documents = new ArrayList<>();

  public Tag() {
  }

  public Tag(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @JsonbTransient
  public List<Document> getDocuments() {
    return documents;
  }

  public void setDocuments(List<Document> documents) {
    this.documents = documents;
  }

  public String getDocumentsUrl() {
    return documentsUrl;
  }

  public void setDocumentsUrl(String documentsUrl) {
    this.documentsUrl = documentsUrl;
  }
}
