package nl.marlevous.documentservice;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class DocumentServiceResourceConfig extends ResourceConfig {

  public DocumentServiceResourceConfig() {
    packages("nl.marlevous.documentservice");
    // Uncomment this if you want extensive jersey logging
    // register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
    // Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
    register(MultiPartFeature.class);
    register(new CORSFilter());

  }
}