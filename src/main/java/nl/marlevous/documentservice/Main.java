package nl.marlevous.documentservice;

import java.io.IOException;
import java.net.URI;
import nl.marlevous.documentservice.database.DatabaseClass;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class.
 *
 */
public class Main {

    static Logger logger = LoggerFactory.getLogger(Main.class);
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // Create handler for static content
        final CLStaticHttpHandler staticHttpHandler = new CLStaticHttpHandler(Main.class.getClassLoader(), "/static/");
        // create a resource config that scans for JAX-RS resources and providers
        // in nl.marlevous.rest package
        //final ResourceConfig rc = new ResourceConfig().packages("nl.marlevous.documentService.resources");
        //rc.register(MultiPartFeature.class);
        final ResourceConfig rc = new DocumentServiceResourceConfig();
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        String base_uri = NodsProperties.getInstance().getProperty("documentService.base_uri");
        logger.info("Starting http server on {}", base_uri);

        HttpServer server =  GrizzlyHttpServerFactory.createHttpServer(URI.create(base_uri), rc);
        server.getServerConfiguration().addHttpHandler(staticHttpHandler);
        return server;
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException, InterruptedException {

        // Make a connection to the database.
        DatabaseClass.connectToDatabase();
        setGrizzlyLog();
        final HttpServer server = startServer();
        logger.info("http server started.");
        while(server.isStarted()) {
            Thread.sleep(100000L);
        }
    }

    private static void setGrizzlyLog() {

        java.util.logging.Logger l = java.util.logging.Logger.getLogger("org.glassfish.grizzly.http.server.HttpServer");
        l.setLevel(java.util.logging.Level.FINE);
        l.setUseParentHandlers(false);
        java.util.logging.ConsoleHandler ch = new java.util.logging.ConsoleHandler();
        ch.setLevel(java.util.logging.Level.ALL);
        l.addHandler(ch);

    }
}

