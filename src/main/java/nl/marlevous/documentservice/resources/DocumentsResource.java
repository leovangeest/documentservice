package nl.marlevous.documentservice.resources;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import nl.marlevous.documentservice.model.Document;
import nl.marlevous.documentservice.model.DocumentCount;
import nl.marlevous.documentservice.services.DocumentService;
import nl.marlevous.documentservice.services.FileService;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/documents")
public class DocumentsResource {

  Logger logger = LoggerFactory.getLogger(DocumentsResource.class);

  DocumentService documentService = new DocumentService();
  FileService fileService = new FileService();



  @GET
  @Produces({ "application/json" })
  public Response getDocuments(
      @QueryParam("offset") @DefaultValue("0") Integer offset,
      @QueryParam("limit")  @DefaultValue("24") Integer limit,
      @QueryParam("search") String search,
      @Context UriInfo info) {

    logger.info("GET {}", info.getAbsolutePath());
    List<Document> documents =  documentService.getAllDocuments(offset, limit, search);
    documents.forEach(p -> setDocumentUrl(p, info));
    Response response = Response
        .status(Status.OK)
        .entity(documents)
        .build();
    return response;
  }

  @GET
  @Path("/count")
  @Produces({ "application/json" })
  public Response getDocumentCount(@QueryParam("search")  String search) {
    DocumentCount count = documentService.getDocumentCount(search);
    return Response.ok().entity(count).build();
  }
  @GET
  @Path("/{documentId}")
  public Response getDocumentById(@PathParam("documentId") Long id, @Context UriInfo info) {

    logger.info("GET {}", info.getAbsolutePath());

    // Get the document for that id
    Document document = documentService.getDocument(id);

    // Fail if wrong
    if(document == null) {
      logger.warn("Document id {} not found", Long.toString(id));
      return Response.status(Status.NOT_FOUND).build();
    }

    File file = fileService.fileForDocument(document);
    logger.info("--> {}", file.getAbsolutePath());
    ResponseBuilder response = Response.ok((Object) file, document.getType());
    // response.header("Content-Disposition", "attachment; filename=\"" + document.getName() + "\"");
    return response.build();
  }

  @POST
  @Produces(MediaType.TEXT_PLAIN)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response upLoad(
      @FormDataParam("tags") String tags,
      @FormDataParam("document") InputStream uploadedInputStream,
      @FormDataParam("document") FormDataContentDisposition fileDetail,
      @FormDataParam("document") final FormDataBodyPart body,
      @Context UriInfo info) {

    logger.info("POST {} file: {} type: {} tags: {}",
        info.getAbsolutePath(),
        fileDetail.getFileName(),
        body.getMediaType().toString(),
        tags);

    Document document = new Document(fileDetail.getFileName(),body.getMediaType().toString());
    documentService.addDocument(document, tags);
    File outputFile = fileService.storeDocument(document, uploadedInputStream);
    String responseText = String.format("document %d stored in %s.", document.getId(), outputFile.getAbsolutePath());
    return Response.status(200).entity(responseText).build();
  }

  static public void setDocumentUrl(Document document, UriInfo uriInfo) {
    //create self link

    UriBuilder builder = uriInfo.getBaseUriBuilder()
        .path(DocumentsResource.class)
        .path(Long.toString(document.getId()));
    Link link = Link.fromUriBuilder(builder).build();
    document.setUrl(link.getUri().toString());
  }

  private void saveToFile(InputStream uploadedInputStream,
      String uploadedFileLocation) {

    try {
      OutputStream out = null;
      int read = 0;
      byte[] bytes = new byte[1024];

      out = new FileOutputStream(new File(uploadedFileLocation));
      while ((read = uploadedInputStream.read(bytes)) != -1) {
        out.write(bytes, 0, read);
      }
      out.flush();
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
