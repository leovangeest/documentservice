package nl.marlevous.documentservice.resources;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import nl.marlevous.documentservice.model.Document;
import nl.marlevous.documentservice.model.Tag;
import nl.marlevous.documentservice.services.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/tags")
public class TagResource {

  Logger logger = LoggerFactory.getLogger(TagResource.class);

  TagService tagService = new TagService();

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getTags(
      @Context UriInfo info) {

    logger.info("GET {}", info.getAbsolutePath());

    List<Tag> tags = tagService.getAllTags();
    tags.forEach(tag -> setTagDocumentsUrl(tag, info));

    Response response = Response.status(Status.OK).entity(tags).build();
    return response;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{tagName}")
  public Response getTag(
      @PathParam("tagName") String tagName,
      @Context UriInfo info) {

    logger.info("GET {}", info.getAbsolutePath());

    Tag tag = tagService.getTag(tagName);
    if (tag == null) {
      return Response.status(Status.NOT_FOUND).build();
    }
    setTagDocumentsUrl(tag, info);
    return Response.status(Status.OK).entity(tag).build();

  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{tagName}/documents")
  public Response getDocumentsForTag(
      @PathParam("tagName") String tagName,
      @Context UriInfo info) {

    logger.info("GET {}", info.getAbsolutePath());

    Tag tag = tagService.getTag(tagName);
    if (tag == null) {
      return Response.status(Status.NOT_FOUND).build();
    }
    List<Document> documents = tag.getDocuments();

    // Provide the urls (HATEOAS)
    documents.forEach(document -> DocumentsResource.setDocumentUrl(document, info));

    return Response.status(Status.OK).entity(documents).build();
  }

  private void setTagDocumentsUrl(Tag tag, UriInfo uriInfo) {
    UriBuilder uriBuilder = uriInfo.getBaseUriBuilder().path(this.getClass()).path(tag.getName())
        .path("documents");
    Link link = Link.fromUriBuilder(uriBuilder).build();
    tag.setDocumentsUrl(link.getUri().toString());
  }
}
