package nl.marlevous.documentservice;

import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodsProperties {

  Logger logger = LoggerFactory.getLogger(NodsProperties.class);

  static private NodsProperties currentInstance;
  private ResourceBundle resourceBundle;

  public NodsProperties() {
    resourceBundle = ResourceBundle.getBundle("app");
    logger.info("Looking for properties in {}.", resourceBundle.getBaseBundleName());


  }

  public static NodsProperties getInstance() {
    if (currentInstance == null) {
      currentInstance = new NodsProperties();
    }
    return currentInstance;
  }

  public String getProperty(String name) {
    return resourceBundle.getString(name);
  }
}
