package nl.marlevous.documentservice.database;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.print.Doc;
import nl.marlevous.documentservice.model.Document;
import nl.marlevous.documentservice.model.Tag;
import nl.marlevous.documentservice.services.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseClass {

  static Logger logger = LoggerFactory.getLogger(FileService.class);

  private static boolean inTransaction = false;

  private static EntityManagerFactory ENTITY_MANAGER_FACTORY;

  private static EntityManager entityManager;

  private static EntityTransaction transaction;

  public static void connectToDatabase() throws InterruptedException {
    boolean connected = false;
    while (!connected) {
      try {
        ENTITY_MANAGER_FACTORY =
            Persistence.createEntityManagerFactory("nods");
        Map<String, Object> props = ENTITY_MANAGER_FACTORY.getProperties();
        connected = true;
      } catch (Exception e) {
        int wait = 5;
        logger.error("Could not connect to database. Retry in {} seconds", wait);
        Thread.sleep(wait*1000);
      }
    }

    entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
    transaction = entityManager.getTransaction();
  }

  public static void persist(Document document) {
    entityManager.persist(document);
  }

  public static void persist(Tag tag) {
    entityManager.persist(tag);
  }

  public static Document getDocument(long id) {
    TypedQuery<Document> query = entityManager.createQuery(
        "SELECT d FROM Document d WHERE d.id = :id", Document.class);
    query.setParameter("id", id);

    List<Document> documents = query.getResultList();
    Document document = null;
    if (documents.size() != 0) {
      document = documents.get(0);
    }
    return document;
  }

  public static List<Document> getAllDocuments(int offset, int limit, String search) {

    String queryString = "select d from Document d";
    boolean addSearch = search != null && !search.isEmpty();

    //TODO: Use Criteria?
    if(addSearch) {
      queryString += " WHERE lower(d.name) like :search";
    }

    if (addSearch) {
      logger.info(
          "Executing query [{}] with offset {},limit {} and search {}",
          queryString,
          offset,
          limit,
          search.toLowerCase());
      }
    else {
      logger.info(
          "Executing query [{}] with offset {} and limit {}",
          queryString,
          offset,
          limit);
    }

    TypedQuery query =  entityManager.createQuery(
        queryString,
        Document.class
    );

    if(addSearch) {
      query.setParameter("search", "%" + search.toLowerCase() + "%");
    }

    query.setFirstResult(offset);
    query.setMaxResults(limit);

    return query.getResultList();
  }

  public static List<Tag> getTags() {
    List<Tag> tags = entityManager.createQuery(
        "SELECT t FROM Tag t",
        Tag.class
    ).getResultList();
    return tags;
  }

  public static void startTransaction() {
    transaction.begin();
    inTransaction = true;
  }

  public static void commitTransaction() {
    transaction.commit();
    inTransaction = false;
  }

  public static boolean isInTransaction() {
    return inTransaction;
  }

  public static Tag getTag(String tagName) {
    TypedQuery<Tag> query = entityManager.createQuery(
        "SELECT t FROM Tag t WHERE t.name = :name", Tag.class);
    query.setParameter("name", tagName);

    List<Tag> tags = query.getResultList();

    Tag tag = new Tag(tagName);
    if (tags.size() != 0) {
      tag = tags.get(0);
    }
    return tag;
  }

  public static Long getDocumentCount(String search) {

    boolean addSearch = search != null && !search.isEmpty();

    String countQueryString = "select count(d) from Document d";
    if(addSearch) {
      countQueryString += " WHERE lower(d.name) like :search";
    }

    TypedQuery<Long> query = entityManager.createQuery(countQueryString,Long.class);
    if(addSearch) {
      query.setParameter("search", "%" + search.toLowerCase() + "%");
    }
    return query.getSingleResult();
  }
}
