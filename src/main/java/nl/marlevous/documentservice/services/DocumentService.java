package nl.marlevous.documentservice.services;

import java.util.List;
import nl.marlevous.documentservice.database.DatabaseClass;
import nl.marlevous.documentservice.model.Document;
import nl.marlevous.documentservice.model.DocumentCount;
import nl.marlevous.documentservice.model.Tag;

public class DocumentService {

  TagService tagService = new TagService();

  public List<Document> getAllDocuments(int offset, int limit, String search) {
    return DatabaseClass.getAllDocuments(offset, limit, search);
  }

  public Document getDocument(Long id) {
    return DatabaseClass.getDocument(id);
  }

  public Document addDocument(Document document, String tagList) {

    DatabaseClass.startTransaction();
    DatabaseClass.persist(document);

    String[] tagNames = tagList.split(",");
    for(String tagName : tagNames) {
      Tag tag = tagService.getTag(tagName);
      tag.getDocuments().add(document);
      tagService.store(tag);
    }

    DatabaseClass.commitTransaction();

    return document;
  }

  public DocumentCount getDocumentCount(String search) {
    DocumentCount result = new DocumentCount();
    result.setCount(DatabaseClass.getDocumentCount(search));
    return result;
  }
}
