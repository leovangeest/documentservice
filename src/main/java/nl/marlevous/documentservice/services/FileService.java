package nl.marlevous.documentservice.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import nl.marlevous.documentservice.NodsProperties;
import nl.marlevous.documentservice.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileService {

  File root;
  Logger logger = LoggerFactory.getLogger(FileService.class);

  public FileService() {
     root =  new File(NodsProperties.getInstance().getProperty("documentService.uploadLocation"));
     if(!root.exists()) {
       logger.info("Root directory {} does not exist. Trying to create it.",root.getAbsolutePath());
       root.mkdirs();
     }
  }

  public File storeDocument(Document document, InputStream stream) {

    // Get the output file in our document structure
    File outputFile = fileForDocument(document);
    logger.info("Store document in {}", outputFile.getAbsolutePath());

    // Create the directory for the file (if it doesn't exist)
    outputFile.getParentFile().mkdirs();

    // Now write the file
    try {
      OutputStream out = null;
      int read = 0;
      byte[] bytes = new byte[1024];

      out = new FileOutputStream(outputFile);
      while ((read = stream.read(bytes)) != -1) {
        out.write(bytes, 0, read);
      }
      out.flush();
      out.close();
    } catch (IOException e) {
      logger.error("storeDocument: Error writing the file {}", outputFile.getAbsolutePath(), e);
    }
    return outputFile;
  }

  public File fileForDocument(Document document) {
    // Determine the directory for the document
    String docId = String.format("%011d", document.getId());

    // Make a 2 level directory of the first 8 digits. This gives 10^8 = 100 million directories to store
    // documents in. Each directory holds 1000 documents. So whe can store 10^11 documents.

    File level1Dir = new File(root,docId.substring(0,4));
    File level2Dir = new File(level1Dir, docId.substring(4,8));
    File documentFile = new File(level2Dir, Long.toString(document.getId()) + "_" + document.getName());

    return documentFile;
  }
  private static String extension(File file) {
    String fileName = file.getName();
    if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
      return fileName.substring(fileName.lastIndexOf(".")+1);
    else return "";
  }
}
