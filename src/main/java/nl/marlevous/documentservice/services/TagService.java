package nl.marlevous.documentservice.services;

import java.util.List;
import nl.marlevous.documentservice.database.DatabaseClass;
import nl.marlevous.documentservice.model.Tag;

public class TagService {

  public List<Tag> getAllTags() {
    DatabaseClass.startTransaction();
    List<Tag> tags = DatabaseClass.getTags();
    DatabaseClass.commitTransaction();
    return tags;
  }

  public Tag getTag(String tagName) {
    Tag tag = DatabaseClass.getTag(tagName);
    return tag;
  }

  public void store(Tag tag) {
    boolean inTransaction = DatabaseClass.isInTransaction();
    if(!inTransaction) {
      DatabaseClass.startTransaction();
    }
    DatabaseClass.persist(tag);
    if(!inTransaction) {
      DatabaseClass.commitTransaction();
    }
  }
}
