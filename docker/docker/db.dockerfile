FROM postgres
ENV POSTGRES_USER nods
ENV POSTGRES_PASSWORD nods
ENV POSTGRES_DB nods
# commented out initialization script
# COPY init.sql /docker-entrypoint-initdb.d/
