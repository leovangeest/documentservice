#!/bin/bash

function curlit() {
  curl -X POST \
    "http://localhost:8080/documentservice/documents" \
    -H "accept: */*" \
    -H "Content-Type: multipart/form-data" \
    -F "tags=$4" \
    -F "document=@$1.$2;type=$3"
    echo ""
}

for file in *
do
  filename=$(basename -- "$file")
  extension="${filename##*.}"
  filename="${filename%.*}"
  mime=$(file -b --mime "${file}")
  tagfile="${filename}.tags"

  if [ "${extension}" ==  "tags" ]
  then
    continue
  fi

  if [ "${extension}" ==  "sh" ]
  then
    continue
  fi

  if [[ -r "${tagfile}" ]]
  then
    sep=""
    tags=""
    while read -r line
    do
      tags="${tags}${sep}${line}"
      sep=","
    done < "${tagfile}"
    unset sep
  fi

  curlit "${filename}" "${extension}" "${mime}" "${tags}"
  unset tags
done
